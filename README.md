Snake Game

This is a basic Snake game. The movements of the snakes are controlled by the user using the arrow keys. The motive of the user is to feed on the snake with food. The size of the snake keeps on increasing by one block each time it eats the food. Simultaneously, the score is being recorded. The score keeps on increasing by one each time the snake eats the food. The game is over if the snake hits the game window boundary.
